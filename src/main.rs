use simple_stopwatch::Stopwatch;
use std::io::Write;

mod pe1;
mod pe10;
mod pe11;
mod pe12;
mod pe13;
mod pe14;
mod pe15;
mod pe16;
mod pe17;
mod pe18;
mod pe19;
mod pe2;
mod pe20;
mod pe21;
mod pe22;
mod pe23;
mod pe24;
mod pe25;
mod pe3;
mod pe4;
mod pe5;
mod pe6;
mod pe67;
mod pe7;
mod pe7_alt;
mod pe8;
mod pe9;
mod vectorcalculator;

fn main() {
    println!("Which one to test?");
    let mut tmpinput: String = "".to_string();
    std::io::stdout().flush().unwrap();
    std::io::stdin().read_line(&mut tmpinput).unwrap();
    let input = tmpinput.trim();
    println!();
    run(input);
}

fn run(project_number: &str) {
    let sw = Stopwatch::start_new();
    match project_number {
        "1" => pe1::main(),
        "2" => pe2::main(),
        "3" => pe3::main(),
        "4" => pe4::main(),
        "5" => pe5::main(),
        "6" => pe6::main(),
        "7" => pe7::main(),
        "7_alt" => pe7_alt::main(),
        "8" => pe8::main(),
        "9" => pe9::main(),
        "10" => pe10::main(),
        "11" => pe11::main(),
        "12" => pe12::main(),
        "13" => pe13::main(),
        "14" => pe14::main(),
        "15" => pe15::main(),
        "16" => pe16::main(),
        "17" => pe17::main(),
        "18" => pe18::main(),
        "19" => pe19::main(),
        "20" => pe20::main(),
        "21" => pe21::main(),
        "22" => pe22::main(),
        "23" => pe23::main(),
        "24" => pe24::main(),
        "25" => pe25::main(),
        "67" => pe67::main(),
        "all" => {
            pe1::main();
            pe2::main();
            pe3::main();
            pe4::main();
            pe5::main();
            pe6::main();
            pe7::main();
            pe7_alt::main();
            pe8::main();
            pe9::main();
            pe10::main();
            pe11::main();
            pe12::main();
            pe13::main();
            pe14::main();
            pe15::main();
            pe16::main();
            pe17::main();
            pe18::main();
            pe19::main();
            pe20::main();
            pe21::main();
            pe22::main();
            pe23::main();
            pe24::main();
            pe25::main();
            pe67::main();
        }
        _ => {
            println!("No solution for that number!");
            std::process::exit(0);
        }
    }
    let time_s = sw.s();
    let time_ms = sw.ms();
    println!();
    if time_s >= 1.0 {
        println!(
            "Took {} minutes / {} seconds / {} miliseconds to execute",
            time_s / 60.0,
            time_s,
            time_ms
        );
    } else {
        println!("Took {} miliseconds to execute", time_ms);
    }
}
