//Using The Sieve of Eratosthenes
//https://youtu.be/Lj_SzTGr-G4
//2 years ago, my solution of this
//same problem was taking +8 minutes
//to calculate
pub fn main() {
    println!("Creating prime number list...");
    let mut numbers: Vec<i32> = vec![];
    let mut counter: i32 = 3;
    while counter < 2000000 {
        numbers.push(counter);
        counter += 2;
    }
    let mut index: usize = 0;
    let mut pass = 1;
    loop {
        let current_prime: i32 = numbers[index];
        let mut index_to_remove: Vec<usize> = vec![];
        for i in index + 1..numbers.len() {
            if numbers[i] % current_prime == 0 {
                index_to_remove.push(i);
            }
        }
        index += 1;
        let mut counter = 0;
        for x in index_to_remove {
            let index = x - counter;
            numbers.remove(index);
            counter += 1;
        }
        println!(
            "Pass: {}, remaining numbers: {}, index: {}",
            pass,
            numbers.len(),
            index
        );
        pass += 1;
        if index >= numbers.len() {
            break;
        }
        if current_prime * current_prime >= numbers[numbers.len() - 1] {
            break;
        }
    }
    let mut answer: u64 = 2;
    for n in numbers {
        answer += n as u64;
    }
    println!();
    println!("pe#10: {}", answer);
}
