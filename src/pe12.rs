pub fn main() {
    println!("Creating the prime number list");
    let mut prime_numbers_without_two: Vec<i32> = vec![];
    let mut counter: i32 = 3;

    while counter < 200000 {
        prime_numbers_without_two.push(counter);
        counter += 2;
    }
    let mut index: usize = 0;
    let mut pass = 1;
    loop {
        let current_prime: i32 = prime_numbers_without_two[index];
        let mut index_to_remove: Vec<usize> = vec![];
        for i in index + 1..prime_numbers_without_two.len() {
            if prime_numbers_without_two[i] % current_prime == 0 {
                index_to_remove.push(i);
            }
        }
        index += 1;
        let mut counter = 0;
        for x in index_to_remove {
            let index = x - counter;
            prime_numbers_without_two.remove(index);
            counter += 1;
        }
        println!(
            "Pass: {}, remaining prime_numbers_without_two: {}, index: {}",
            pass,
            prime_numbers_without_two.len(),
            index
        );
        pass += 1;
        if index >= prime_numbers_without_two.len() {
            break;
        }
        if current_prime * current_prime
            >= prime_numbers_without_two[prime_numbers_without_two.len() - 1]
        {
            break;
        }
    }
    println!("Prime number list got created.");

    let mut prime_numbers: Vec<i32> = vec![];
    prime_numbers.push(2);
    prime_numbers.append(&mut prime_numbers_without_two);

    let mut number: u128 = 1;
    let mut num_to_add: u128 = 2;

    loop {
        number += num_to_add;
        num_to_add += 1;
        let mut tmp_num: u128 = number;
        let mut prime_divisor_counter: Vec<i32> = vec![];

        for i in 0..prime_numbers.len() {
            let mut counter = 0;
            while (prime_numbers[i] as u128) <= tmp_num && tmp_num % prime_numbers[i] as u128 == 0 {
                tmp_num /= prime_numbers[i] as u128;
                counter += 1;
            }
            if counter != 0 {
                prime_divisor_counter.push(counter);
            }
            if prime_numbers[i] as u128 > tmp_num {
                break;
            }
        }
        let mut total = 1;
        for n in &prime_divisor_counter {
            total *= n + 1;
        }
        if total > 500 {
            break;
        }
        prime_divisor_counter.clear();
    }
    println!();
    println!("pe#12: {}", number);
}
