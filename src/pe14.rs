pub fn main() {
    let mut longest: u128 = 0;
    let mut longest_num: u128 = 0;
    for i in 2..1000000 {
        let mut x = i;
        let seq: u128 = sequence(&mut x);
        if seq > longest {
            longest = seq;
            longest_num = i;
        }
    }
    println!("pe#14: {}", longest_num);
}

fn sequence(num: &mut u128) -> u128 {
    let mut length: u128 = 0;
    while *num != 1 {
        if *num % 2 == 0 {
            *num /= 2;
        } else {
            *num = 3 * *num + 1;
        }
        length += 1;
    }
    return length;
}
