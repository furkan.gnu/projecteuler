//used combinatorics to solve

pub fn main() {
    let mut answer: u128 = 1;
    let mut tf: u128 = 1;
    for i in 1..21 {
        tf *= i;
    }
    for i in 21..41 {
        answer *= i;
    }
    answer /= tf;
    println!("pe#15: {}", answer);
}
