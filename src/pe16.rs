pub fn main() {
    let mut power: u16 = 1;
    let mut digits: Vec<i32> = vec![];
    digits.push(2);
    let mut hand: i32 = 0;
    while power < 1000 {
        let output = double(&mut digits, hand);
        digits = output.0;
        hand = output.1;
        power += 1;
    }
    let mut answer = 0;
    for n in digits {
        answer += n;
    }
    println!("pe#16: {}", answer);
}

fn double(digits: &mut Vec<i32>, mut hand: i32) -> (Vec<i32>, i32) {
    let mut return_vector: Vec<i32> = vec![];
    let mut reverse_return_vector: Vec<i32> = vec![];
    while !digits.is_empty() {
        let number: i32 = (digits.pop().unwrap() * 2) + hand;
        hand = 0;
        if number > 9 {
            hand = number / 10;
        }
        reverse_return_vector.push(number % 10);
    }
    if hand != 0 {
        reverse_return_vector.push(hand);
        hand = 0;
    }

    while !reverse_return_vector.is_empty() {
        return_vector.push(reverse_return_vector.pop().unwrap());
    }
    return (return_vector, hand);
}
