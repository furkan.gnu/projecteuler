pub fn main() {
    let mut sum: i32 = 0;
    for n in 1..1001 {
        sum += calculate(n);
    }
    println!("pe#17: {}", sum);
}

fn calculate(mut number: i32) -> i32 {
    let mut string: String = "".to_string();
    let mut sum: i32 = 0;
    let mut ones: u8 = 0;
    let mut tens: u8 = 0;
    let mut hundreds: u8 = 0;
    let mut thousands: u8 = 0;
    let mut counter: u8 = 0;
    while number != 0 {
        let last_digit: u8 = (number % 10) as u8;
        match counter {
            0 => ones = last_digit,
            1 => tens = last_digit,
            2 => hundreds = last_digit,
            3 => thousands = last_digit,
            _ => {}
        }
        counter += 1;
        number /= 10;
    }
    let mut skip_tens_and_ones: bool = false;
    if tens == 1 {
        skip_tens_and_ones = true;
        match ones {
            0 => string += "ten",
            1 => string += "eleven",
            2 => string += "twelve",
            3 => string += "thirteen",
            4 => string += "fourteen",
            5 => string += "fifteen",
            6 => string += "sixteen",
            7 => string += "seventeen",
            8 => string += "eighteen",
            9 => string += "nineteen",
            _ => {}
        }
    }
    if thousands == 1 {
        string += "one thousand";
    }
    match hundreds {
        1 => string += "one hundred",
        2 => string += "two hundred",
        3 => string += "three hundred",
        4 => string += "four hundred",
        5 => string += "five hundred",
        6 => string += "six hundred",
        7 => string += "seven hundred",
        8 => string += "eight hundred",
        9 => string += "nine hundred",
        _ => {}
    }
    if hundreds > 0 && ones + tens != 0 {
        string += " and ";
    }
    if !skip_tens_and_ones {
        match tens {
            2 => string += "twenty",
            3 => string += "thirty",
            4 => string += "forty",
            5 => string += "fifty",
            6 => string += "sixty",
            7 => string += "seventy",
            8 => string += "eighty",
            9 => string += "ninety",
            _ => {}
        }
        match ones {
            1 => string += "one",
            2 => string += "two",
            3 => string += "three",
            4 => string += "four",
            5 => string += "five",
            6 => string += "six",
            7 => string += "seven",
            8 => string += "eight",
            9 => string += "nine",
            _ => {}
        }
    }
    for c in string.chars() {
        if c != ' ' {
            sum += 1;
        }
    }
    return sum;
}
