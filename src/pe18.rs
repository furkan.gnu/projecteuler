// Same as #67

pub fn main() {
    let string: String = "75
    95 64
    17 47 82
    18 35 87 10
    20 04 82 47 65
    19 01 23 75 03 34
    88 02 77 73 07 63 67
    99 65 04 28 06 16 70 92
    41 41 26 56 83 40 80 70 33
    41 48 72 33 47 32 37 16 94 29
    53 71 44 65 25 43 91 52 97 51 14
    70 11 33 28 77 73 17 78 39 68 17 57
    91 71 52 38 17 14 91 43 58 50 27 29 48
    63 66 04 68 89 53 67 30 73 16 69 87 40 31
    04 62 98 27 23 09 70 98 73 93 38 53 60 04 23"
        .to_string();
    let mut linevector: Vec<Vec<i32>> = vec![];
    for line in string.lines() {
        let mut tmpvec: Vec<i32> = vec![];
        for number in line.split(' ') {
            match number.parse::<i32>() {
                Ok(o) => {
                    tmpvec.push(o);
                }
                Err(_) => {}
            }
        }
        linevector.push(tmpvec);
    }
    let mut bottomvec: Vec<i32> = linevector.pop().unwrap();
    let mut topvec: Vec<i32> = linevector.pop().unwrap();
    while !linevector.is_empty() {
        summer(&mut topvec, &mut bottomvec, &mut linevector);
    }
    let mut biggest: i32 = 0;
    for number in bottomvec {
        if number > biggest {
            biggest = number;
        }
    }
    biggest += 75;
    println!("pe#18: {}", biggest);
}

fn summer(topvec: &mut Vec<i32>, bottomvec: &mut Vec<i32>, linevector: &mut Vec<Vec<i32>>) {
    let mut newvec: Vec<i32> = vec![];
    for t in 0..topvec.len() {
        if topvec[t] + bottomvec[t] > topvec[t] + bottomvec[t + 1] {
            newvec.push(topvec[t] + bottomvec[t]);
        } else {
            newvec.push(topvec[t] + bottomvec[t + 1]);
        }
    }
    *bottomvec = newvec;
    *topvec = (*linevector.pop().unwrap()).to_vec();
}
