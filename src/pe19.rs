pub fn main() {
    let mut year: i32 = 1900;
    let mut month: u8 = 1;
    let mut day_of_week: u8 = 1;
    let mut counter: u32 = 0;
    while year < 2001 {
        let delta_days: u8 = calculate_first_day_of_month(year, month);
        day_of_week += delta_days;
        if day_of_week > 7 {
            day_of_week -= 7;
        }
        if day_of_week == 7 && year > 1900 {
            counter += 1;
        }
        month += 1;
        if month == 13 {
            month = 1;
            year += 1;
        }
    }
    println!("pe#19: {}", counter);
}

fn is_leap_year(year: i32) -> bool {
    if year % 4 == 0 && year % 400 == 0 {
        return true;
    } else {
        return false;
    }
}

fn calculate_first_day_of_month(year: i32, month: u8) -> u8 {
    let days: u8;
    match month {
        6 | 9 | 4 | 11 => days = 30,
        2 => {
            if is_leap_year(year) {
                days = 29;
            } else {
                days = 28;
            }
        }
        _ => days = 31,
    }
    let day_of_week: u8 = days / 7;
    return day_of_week;
}
