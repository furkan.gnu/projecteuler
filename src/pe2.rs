pub fn main() {
    let mut first = 1;
    let mut second = 2;
    let mut sum = 0;
    while second < 4000000 {
        if second % 2 == 0 {
            sum += second
        }
        second = first + second;
        first = second - first;
    }
    println!("pe#2: {}", sum);
}
