use std::vec;

use crate::vectorcalculator;

pub fn main() {
    let mut all_numbers: Vec<Vec<u8>> = vec![];
    for mut i in 1..101 {
        let mut number: Vec<u8> = vec![];
        while i > 0 {
            number.push((i % 10) as u8);
            i /= 10;
        }
        all_numbers.push(number.into_iter().rev().collect());
    }
    let mut sum: Vec<u8> = vec![1];
    for i in all_numbers {
        sum = vectorcalculator::multiply(sum, i);
    }

    let mut answer: u128 = 0;
    for i in &sum {
        answer += *i as u128;
    }
    println!("pe#20: {}", answer);
    print!("100! -> ");
    vectorcalculator::print_number_newline(&sum);
}
