use std::vec;

pub fn main() {
    let mut vector: Vec<u128> = vec![];
    vector.push(0);
    for k in 1..10001 {
        let mut sum: u128 = 0;
        for l in 1..k {
            if k % l == 0 {
                sum += l;
            }
        }
        vector.push(sum);
    }
    for i in 1..100000 {
        vector.push(0);
    }
    let mut sum: u128 = 0;
    for i in 1..10001 {
        let right = vector[i];
        let left = vector[right as usize];
        /*
            We are adding left != right because if left = right, ex: 6 = 6, then we would be doing
            a mistake because 6's sum of proper divisors are equal to 6. This also applies for: 28 and 8128
            etc.

            left here is the first number, being the index of vector
            right here is the sum of all that number's proper divisors

            we added 0 at the element 0 at the line #5 of the code so the vector[1] can be equal to sum of
            the proper divisors of 1. We used the i in vector[i] as our index value in 0..10001 and the value it points
            to as our sum so we can easily compare the values.
        */
        if left == i as u128 && left != right {
            sum += i as u128;
        }
    }
    println!("pe#21: {}", sum);
}
