use std::fs;

pub fn main() {
    let mut namestxt = fs::read_to_string("src/names.txt").unwrap();
    namestxt = "\",".to_string() + &namestxt; // Making the text file start with "," to get in line with the splitting argument.
    namestxt.pop(); // Removing the " at the end of the file.
    let mut names: Vec<String> = vec![];
    for name in namestxt.split("\",\"") {
        // This pushes "".to_string() into the names[0] for a reason. Uncomment the below code to see:
        names.push(name.to_string());
    }
    names.sort();
    let mut score_sum: u128 = 0;
    for i in 0..names.len() {
        /*
        // You can uncomment the below code to see that names[0] = "".to_string() I don't know what causes it to be there.
        if i < 3 {
            println!("{} - {} - {}", i,&names[i],calculate_score(&names[i]));
        }
        */
        score_sum += i as u128 * calculate_score(&names[i]);
    }
    let mut max_name: &String = &"".to_string();
    let mut max_score: u128 = 0;
    for i in 0..names.len() {
        let score: u128 = calculate_score(&names[i]);
        println!("{} - {} - {} - {}", i, names[i], score, i as u128 * score);
        if i as u128 * score > max_score {
            max_score = i as u128 * score;
            max_name = &names[i];
        }
    }
    println!();
    println!(
        "Fun fact: The name with the max. score is {} with {} score.",
        max_name, max_score
    );

    println!("pe#22: {}", score_sum);
}

fn calculate_score(name: &String) -> u128 {
    let mut score: u128 = 0;
    for c in name.chars() {
        score += (c as u8 - 64) as u128;
    }
    score
}
