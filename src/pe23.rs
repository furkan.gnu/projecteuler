// answer: 4179871

pub fn main() {
    println!("Creating sums vector...");
    let mut sums: Vec<u128> = vec![];
    for k in 0..28124 {
        let mut sum: u128 = 0;
        for l in 1..k {
            if k % l == 0 {
                sum += l;
            }
        }
        sums.push(sum);
    }
    println!("Creating abundant_numbers vector...");
    let mut abundant_numbers: Vec<Number> = vec![];
    for k in 0..sums.len() {
        let number: Number = Number {
            left: k as u128,
            right: sums[k],
        };
        if number.left < number.right {
            abundant_numbers.push(number);
        }
    }
    println!("Creating sum_of_abundant_numbers vector...");
    let mut sum_of_abundant_numbers: Vec<u128> = vec![];
    for k in &abundant_numbers {
        for l in &abundant_numbers {
            sum_of_abundant_numbers.push(k.left + l.left);
        }
    }
    println!("Sorting sum_of_abundant_numbers vector...");
    sum_of_abundant_numbers.sort();
    // 48511225 elements before deduplication
    println!("Deduplicating sum_of_abundant_numbers vector...");
    sum_of_abundant_numbers.dedup();
    // 53871 elements after deduplication, speeds up the
    //binary search by another 10 seconds.
    let mut sum: u128 = 0;
    println!("Using binary search to find the values...");
    for i in 1..28124 {
        // Using binary search here speeds up the process by 6 times -> 180 secs to 30 secs
        match sum_of_abundant_numbers.binary_search(&i) {
            Err(_) => {
                sum += i;
            }
            _ => {}
        }
    }
    println!("pe#23: {}", sum);
}
struct Number {
    left: u128,
    right: u128,
}
