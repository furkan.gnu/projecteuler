pub fn main() {
    // for nth decimal place to shift, (n - 1)! permutations need to be passed.
    let TWOFACT = 2;
    let THREEFACT = TWOFACT * 3;
    let FOURFACT = THREEFACT * 4;
    let FIVEFACT = FOURFACT * 5;
    let SIXFACT = FIVEFACT * 6;
    let SEVENFACT = SIXFACT * 7;
    let EIGHTFACT = SEVENFACT * 8;
    let NINEFACT = EIGHTFACT * 9;
    let mut digits: Vec<u8> = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    let mut nth = 1000000;
    let mut j = 0;
    let mut k = 0;
    let mut l = 0;
    let mut previous = NINEFACT;
    println!("Calculating permutations for pe#24:");
    while nth > 0 {
        if nth >= NINEFACT {
            nth -= NINEFACT;
            j = 0;
            k = 1 + l;
            l += 1;
        } else if nth >= EIGHTFACT {
            if previous == NINEFACT {
                l = 0;
            }
            j = 1;
            k = 2 + l;
            l += 1;
            nth -= EIGHTFACT;
            previous = EIGHTFACT;
        } else if nth >= SEVENFACT {
            if previous == EIGHTFACT {
                l = 0;
            }
            j = 2;
            k = 3 + l;
            l += 1;
            nth -= SEVENFACT;
            previous = SEVENFACT;
        } else if nth >= SIXFACT {
            if previous == SEVENFACT {
                l = 0;
            }
            j = 3;
            k = 4 + l;
            l += 1;
            nth -= SIXFACT;
            previous = SIXFACT;
        } else if nth >= FIVEFACT {
            if previous == SIXFACT {
                l = 0;
            }
            j = 4;
            k = 5 + l;
            l += 1;
            nth -= FIVEFACT;
            previous = FIVEFACT;
        } else if nth >= FOURFACT {
            if previous == FIVEFACT {
                l = 0;
            }
            j = 5;
            k = 6 + l;
            l += 1;
            nth -= FOURFACT;
            previous = FOURFACT;
        } else if nth >= THREEFACT {
            if previous == FOURFACT {
                l = 0;
            }
            j = 6;
            k = 7 + l;
            l += 1;
            nth -= THREEFACT;
            previous = THREEFACT;
        } else if nth > TWOFACT {
            if previous == THREEFACT {
                l = 0;
            }
            j = 7;
            k = 8 + l;
            l += 1;
            nth -= TWOFACT;
        } else if nth == TWOFACT {
            nth -= TWOFACT;
            j = 8;
            k = 9;
        }
        digits.swap(j, k);
        for n in &digits {
            print!("{}", n);
        }
        println!();
    }
    print!("pe#24: ");
    for n in digits {
        print!("{}", n);
    }
    println!();
}
