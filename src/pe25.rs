// This code can be faster if fib() function does not calculate the fibonacci
// number at every index of i and stored them at a vector for example to use as
// the value f0 and f1 in the fib() function

/*
   TAKES 51 MINUTES TO EXECUTE AND THE ANSWER(46050) IS WRONG!
*/

use num::BigUint;
use num::{One, Zero};
use std::mem::replace;

fn fib(n: u128) -> BigUint {
    let mut f0: BigUint = Zero::zero();
    let mut f1: BigUint = One::one();
    for _ in 0..n {
        let f2 = f0 + &f1;
        f0 = replace(&mut f1, f2);
    }
    f0
}

pub fn main() {
    let mut i: u128 = 0;
    let mut digit_num: u16 = 1;
    while true {
        let x = fib(i);
        if x.to_u32_digits().len() as u16 > digit_num {
            digit_num = x.to_u32_digits().len() as u16;
            println!("Digit number: {}/1000", digit_num);
        }
        if digit_num == 1000 {
            println!("pe#25: {}", i);
            break;
        }
        i += 1;
    }
}
