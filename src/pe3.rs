pub fn main() {
    let mut number: u64 = 600851475143;
    let mut divider: u64 = 2;
    while number > 1 {
        if number % divider == 0 {
            number /= divider;
        } else {
            divider += 1;
        }
    }
    println!("pe#3: {}", divider);
}
