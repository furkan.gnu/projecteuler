pub fn main() {
    let mut answer: u32 = 0;
    for ln in 100..1000 {
        for rn in 100..1000 {
            let palindrome: u32 = ln * rn;
            let mut temp_number: u32 = palindrome;
            let mut reverse_palindrome: u32 = 0;
            while temp_number > 0 {
                reverse_palindrome += temp_number % 10;
                reverse_palindrome *= 10;
                temp_number /= 10;
                if temp_number == 0 {
                    reverse_palindrome /= 10;
                }
            }
            if reverse_palindrome == palindrome && palindrome > answer {
                answer = palindrome;
            }
        }
    }
    println!("pe#4: {}", answer);
}
