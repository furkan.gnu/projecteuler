#[allow(unused_assignments)]

pub fn main() {
    let mut vector1: Vec<i32> = vec![];
    let mut vector2: Vec<i32> = vec![];
    let mut vector3: Vec<i32> = vec![];
    let mut divisor: i32 = 2;
    for i in 2..21 {
        vector1.push(i);
    }
    loop {
        let mut counter: u8 = 0;

        for x in &vector1 {
            if x % divisor == 0 {
                vector2.push(x / divisor);
                counter += 1;
            } else {
                vector2.push(*x);
            }
        }
        if counter > 0 {
            vector3.push(divisor);
        }
        vector1.clear();
        counter = 0;
        for x in &vector2 {
            if x % divisor == 0 {
                vector1.push(x / divisor);
                counter += 1;
            } else {
                vector1.push(*x);
            }
        }
        if counter > 0 {
            vector3.push(divisor);
        } else {
            if divisor > 20 {
                break;
            }
            divisor += 1;
            counter = 0;
        }
        vector2.clear();
        counter = 0;
    }
    let mut answer: i32 = 1;
    for x in vector3 {
        answer *= x;
    }
    println!("pe#5: {}", answer);
}
