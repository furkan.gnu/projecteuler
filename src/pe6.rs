pub fn main() {
    let mut squaresum: i32 = 0;
    let mut sumsquare: i32 = 0;
    for x in 1..101 {
        sumsquare += x;
        squaresum += x * x;
    }
    sumsquare *= sumsquare;
    let answer: i32 = sumsquare - squaresum;
    println!("pe#6: {}", answer);
}
