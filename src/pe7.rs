//This is the way I thought that would be faster,
//I was manually defining exceptions. The alternative
//one which was only using bruteforce is faster :/

#[allow(unused_assignments)]

pub fn main() {
    let mut primevector: Vec<i32> = vec![];
    primevector.push(2);
    primevector.push(3);
    primevector.push(5);
    let mut divider = 3;
    let mut number: i32 = 7;
    while primevector.len() < 10001 {
        let mut is_prime: bool = true;
        divider = 3;
        if number % 5 == 0 {
            number += 2;
            is_prime = false;
        }
        for x in &primevector {
            if number % x == 0 {
                number += 2;
                is_prime = false;
                break;
            }
        }
        while divider < number {
            if number % divider == 0 {
                number += 2;
                is_prime = false;
                break;
            } else {
                divider += 2;
                if divider % 5 == 0 {
                    divider += 2;
                }
            }
        }
        if is_prime {
            primevector.push(number);
        }
    }
    println!("pe#7: {}", primevector.pop().unwrap());
}
