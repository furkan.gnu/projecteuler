pub fn main() {
    let mut primevector: Vec<i32> = vec![];
    primevector.push(2);
    primevector.push(3);
    primevector.push(5);
    let mut number = 7;
    let mut divider = 3;
    while primevector.len() < 10001 {
        let mut is_prime: bool = true;
        while divider < number {
            if number % divider == 0 {
                is_prime = false;
                break;
            } else {
                divider += 2;
            }
        }

        if is_prime {
            primevector.push(number);
        }
        number += 2;
        divider = 3;
    }
    println!("pe#7_alt: {}", primevector.pop().unwrap());
}
