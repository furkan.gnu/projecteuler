pub fn main() {
    let mut found: bool = false;
    for a in 1..1001 {
        for b in 1..1001 {
            let mut c: f64 = (a * a + b * b) as f64;
            c = c.sqrt();
            if a as f64 + b as f64 + c == 1000.0 {
                let answer = a as f64 * b as f64 * c;
                println!("pe#9: {}", answer);
                found = true;
            }
            if found {
                break;
            }
        }
        if found {
            break;
        }
    }
}
