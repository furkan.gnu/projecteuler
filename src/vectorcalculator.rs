use std::thread;
use std::thread::JoinHandle;

pub fn multiply_multithreaded(mut all_numbers: Vec<Vec<u8>>, thread_number: usize) -> Vec<Vec<u8>> {
    let ALL_NUMBERS_LENGTH = all_numbers.len();
    let mut thread_vector: Vec<JoinHandle<Vec<u8>>> = vec![];
    while !all_numbers.is_empty() {
        let mut pnumbers: Vec<Vec<u8>> = vec![];
        for _i in 1..(ALL_NUMBERS_LENGTH + 1) / thread_number {
            if !all_numbers.is_empty() {
                pnumbers.push(all_numbers.pop().unwrap().to_vec());
            }
        }
        let p = thread::spawn(move || {
            let mut psum: Vec<u8> = vec![1];
            for i in pnumbers {
                psum = multiply(psum, i);
            }
            psum
        });
        thread_vector.push(p);
    }
    let mut return_vector: Vec<Vec<u8>> = vec![];
    for i in thread_vector {
        return_vector.push(i.join().unwrap());
    }
    return_vector
}

pub fn add(mut left: Vec<u8>, mut right: Vec<u8>) -> Vec<u8> {
    let mut sum: Vec<u8> = vec![];
    let mut carry: u8 = 0;
    while !left.is_empty() || !right.is_empty() || carry > 0 {
        let mut left_digit: u8 = 0;
        let mut right_digit: u8 = 0;
        if !left.is_empty() {
            left_digit = left.pop().unwrap();
        }
        if !right.is_empty() {
            right_digit = right.pop().unwrap();
        }
        let digits_sum: u8 = right_digit + left_digit + carry;
        carry = 0;
        sum.push(digits_sum % 10);
        if digits_sum > 9 {
            carry = digits_sum / 10;
        }
    }
    sum.into_iter().rev().collect()
}

pub fn multiply(mut left: Vec<u8>, mut right: Vec<u8>) -> Vec<u8> {
    let mut sum: Vec<u8> = vec![];
    while !right.is_empty() {
        let mut right_digit: u8 = right.pop().unwrap();
        while right_digit > 0 {
            right_digit -= 1;
            sum = add(left.to_vec(), sum);
        }
        left.push(0);
    }
    sum
}

pub fn print_number(vector: &Vec<u8>) {
    for i in vector {
        print!("{}", i);
    }
}

pub fn print_number_newline(vector: &Vec<u8>) {
    print_number(vector);
    println!();
}
